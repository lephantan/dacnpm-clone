import qs from 'qs'
import { universityMutations } from './mutations'

export const universityActions = {
  FETCH: {
    DATA: 'university/fetchData',
    MORE: 'university/fetchMoreData',
    SINGLE: 'university/fetchSingle',
  },
  SUBMIT: {
    // MULTIPLE: 'university/submitMultiple',
    SINGLE: 'university/submitSingle',
  },
  UPDATE: {
    // MULTIPLE: 'university/updateMultiple',
    SINGLE: 'university/updateSingle',
  },
  TOGGLE: {},
  ACTIVATE: {},
  DEACTIVATE: {},
  DELETE: {
    // MULTIPLE: 'university/deleteMultiple',
    SINGLE: 'university/deleteSingle',
  },
}

export default {
  async fetchData({ state, commit }) {
    const response = await this.$clientApi.get(
      'universities?' + qs.stringify(state.query, { arrayFormat: 'repeat' })
    )
    commit(universityMutations.SET.DATA, response.data.data, { root: true })
    // Fix total later
    commit(universityMutations.SET.TOTAL, response.data.total, { root: true })
    return response.data
  },
  async fetchMoreData() {
    const response = await this.$clientApi.get('/universities')
    return response
  },
  async fetchSingle({ rootState }, id) {
    const response = await this.$clientApi.get('/universities/' + id)
    return response
  },
  async submitSingle({ rootState }, form) {
    const response = await this.$authApi.post('/universities', form)
    return response
  },
  async updateSingle({ rootState }, { id, form }) {
    const response = await this.$authApi.put('/universities/' + id, form)
    return response
  },
  async deleteSingle({ rootState }, id) {
    const response = await this.$authApi.delete('/universities/' + id)
    return response
  },
}
