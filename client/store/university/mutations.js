import Vue from 'vue'

export const universityMutations = {
  SET: {
    DATA: 'university/SET_DATA',
    TOTAL: 'university/SET_TOTAL',
    QUERY: 'university/SET_QUERY',
  },
  CLEAR: {
    QUERY: 'university/CLEAR_QUERY',
  },
  TOGGLE: {},
  ADD: {},
  REMOVE: {},
  INC: {
    QUERY_PAGE: 'university/INC_QUERY_PAGE',
  },
  SUB: {
    QUERY_PAGE: 'university/SUB_QUERY_PAGE',
  },
}

export default {
  SET_DATA(state, data) {
    Vue.set(state, 'data', data)
  },
  SET_TOTAL(state, total) {
    Vue.set(state, 'total', total)
  },
  SET_QUERY(state, query) {
    Vue.set(state, 'query', { ...state.query, ...query })
  },
  CLEAR_QUERY(state) {
    Vue.set(state, 'query', {
      offset: 0,
      limit: 10,
    })
  },
  INC_QUERY_PAGE(state) {
    Vue.set(state.query, 'page', state.query.page + 1)
  },
  SUB_QUERY_PAGE(state) {
    Vue.set(state.query, 'page', state.query.page - 1)
  },
}
