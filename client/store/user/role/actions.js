import qs from 'qs'
import { roleMutations } from './mutations'

export const roleActions = {
  FETCH: {
    DATA: 'user/role/fetchData',
    MORE: 'user/role/fetchMoreData',
    SINGLE: 'user/role/fetchSingle',
  },
  SUBMIT: {
    // MULTIPLE: 'user/role/submitMultiple',
    SINGLE: 'user/role/submitSingle',
  },
  UPDATE: {
    // MULTIPLE: 'user/role/updateMultiple',
    SINGLE: 'user/role/updateSingle',
  },
  TOGGLE: {},
  ACTIVATE: {},
  DEACTIVATE: {},
  DELETE: {
    // MULTIPLE: 'user/role/deleteMultiple',
    SINGLE: 'user/role/deleteSingle',
  },
}

export default {
  async fetchData({ state, commit }) {
    const response = await this.$authApi.get(
      'roles?' + qs.stringify(state.query, { arrayFormat: 'repeat' })
    )
    commit(roleMutations.SET.DATA, response.data.data, { root: true })
    // Fix total later
    commit(roleMutations.SET.TOTAL, response.data.total, { root: true })
    return response.data
  },
  async fetchMoreData() {
    const response = await this.$authApi.get('/roles')
    return response
  },
  async fetchSingle({ rootState }, id) {
    const response = await this.$authApi.get('/roles/' + id)
    return response
  },
  async submitSingle({ rootState }, form) {
    const response = await this.$authApi.post('/roles', form)
    return response
  },
  async updateSingle({ rootState }, { id, form }) {
    const response = await this.$authApi.put('/roles/' + id, form)
    return response
  },
  async deleteSingle({ rootState }, id) {
    const response = await this.$authApi.delete('/roles/' + id)
    return response
  },
}
