import Vue from 'vue'
import { ROLE_CATEGORIZING } from '~/constants/config/base/auth'

/**
 * Auth mutation module's enum
 */
export const authMutations = {
  SET: {
    AUTH: 'auth/SET_AUTH',
    AUTH_FULLNAME: 'auth/SET_AUTH_FULLNAME',
    AUTH_AVATAR: 'auth/SET_AUTH_AVATAR',
    ROLE_GROUP: 'auth/SET_ROLE_GROUP',
  },
  CLEAR: {},
  TOGGLE: {},
  ADD: {},
  REMOVE: {},
  INC: {},
  SUB: {},
}

export default {
  SET_AUTH(state, auth) {
    Vue.set(state, 'data', auth)
    localStorage.setItem('auth', JSON.stringify(auth))
  },
  SET_ROLE_GROUP(state) {
    Vue.set(
      state,
      'roleGroup',
      ROLE_CATEGORIZING[state.data?.role.name || 'CORE_TEAM_MEMBER']
    )
    localStorage.setItem(
      'roleGroup',
      ROLE_CATEGORIZING[state.data?.role.name || 'CORE_TEAM_MEMBER']
    )
  },
}
