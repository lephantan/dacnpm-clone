// VueX-State
export default () => ({
  // Auth instance got from api server
  data: null,
  roleGroup: 'core-team-member',
})
