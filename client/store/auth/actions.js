import { authMutations } from './mutations'

/**
 * Auth action module's enum
 */
export const authActions = {
  LOGIN: 'auth/login',
  LOGOUT: 'auth/logout',
}

export default {
  /**
   * Login action
   * @param {Object} context Vuex default action's first parameter
   * @param {Object} form Get from login form
   * @param {String} form.email
   * @param {String} form.password
   * @returns {void} Return nothing
   */
  async login({ commit }, form) {
    const { data } = await this.$clientApi.post('/auth/login', form)
    commit(authMutations.SET.AUTH, data, { root: true }) // Mutating to store for client rendering
    commit(authMutations.SET.ROLE_GROUP, {}, { root: true })
  },
  /**
   * Logout action
   * @param {Object} context Vuex default action's first parameter
   * @returns {void} Return nothing
   */
  logout({ commit }) {
    localStorage.removeItem('auth')
    commit(authMutations.SET.AUTH, null, { root: true })
  },
}
