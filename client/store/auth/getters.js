export const authGetters = {
  /**
   * Check if current logged in user is admin or not
   */
  IS_ADMIN: 'auth/isAdmin',
  /**
   * Get current user's role group
   */
  ROLE_GROUP: 'auth/roleGroup',
}
export default {
  /**
   * Should be modified base on backend
   * Check if current logged in user is admin or not
   * @param {*} state
   * @returns {Boolean} true or false
   */
  isAdmin(state) {
    return state.data?.role?.name.includes('ADMIN')
  },
  /**
   *
   * @param {*} state
   * @returns {String} current user's role group
   */
  roleGroup(state) {
    return state?.roleGroup
  },
}
