export default {
  title: 'Log in',
  email: 'Email',
  password: 'Password',
  'email-placeholder': 'Your email...',
  'password-placeholder': 'Your password...',
  button: 'Log in',
}
