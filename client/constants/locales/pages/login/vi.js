export default {
  title: 'Đăng nhập',
  email: 'Email',
  password: 'Mật khẩu',
  'email-placeholder': 'Nhập email của bạn vào...',
  'password-placeholder': 'Nhập mật khẩu của bạn vào...',
  button: 'Đăng nhập',
}
