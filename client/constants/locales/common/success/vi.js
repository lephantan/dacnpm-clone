export default {
  create: 'Tạo mới thành công',
  updated: 'Cập nhật thành công',
  delete: 'Xóa thành công',
  restore: 'Phục hồi thành công',
}
