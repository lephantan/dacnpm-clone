export default {
  create: 'Created successfully',
  updated: 'Updated successfully',
  delete: 'Deleted successfully',
  restore: 'Restored successfully',
}
