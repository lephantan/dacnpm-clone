export default {
  'leave-message':
    'Bạn có muốn rời khỏi trang này? Dữ liệu chưa lưu có thể bị mất!',
  'leave-title': 'Bạn có chắc không?',
  'leave-ok': 'Rời khỏi trang này',
  'leave-abort': 'Hủy bỏ',
}
