export default {
  'leave-message':
    'Do you really want to leave? You can lose your unsaved changes!',
  'leave-title': 'Are you sure?',
  'leave-ok': 'Leave page',
  'leave-abort': 'Cancel',
}
