/**
 * Collection of available permissions
 *
 * You must use these permissions for your <auth> wrapper or v-auth directive
 * @readonly Do not modify
 */
const permissions = [
  'CREATE_STUFF',
  'UPDATE_STUFF',
  'DELETE_STUFF',
  'ALL',
  'SELF',
]
/**
 * SUPER_ADMIN permission
 * @readonly Do not modify
 */
const SUPER_ADMIN = ['ALL']
/**
 * RECEPTION permission
 * @readonly Do not modify
 */
const CORE_TEAM_MEMBER = ['CREATE_STUFF', 'UPDATE_STUFF', 'DELETE_STUFF']
/**
 * Normal user permission
 * @readonly Do not modify
 */
const ALL = ['ALL']
/**
 * Only if user is the author of item
 * @readonly Do not modify
 */
const SELF = ['SELF']

/**
 * Role group
 */
const ROLE_CATEGORIZING = {
  CORE_TEAM_MEMBER: 'core-team-member',
  'Super Admin': 'super-admin',
}

export {
  permissions,
  ROLE_CATEGORIZING,
  SUPER_ADMIN,
  CORE_TEAM_MEMBER,
  ALL,
  SELF,
}
