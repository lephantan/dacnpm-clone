import { ALL } from './auth'

export const sidebar = {
  config: {},
  groups: [
    {
      name: 'management',
      components: [
        {
          module: 'user', // Module name for localization
          icon: ['fas', 'users'], // Fontawesome or Element UI icon
          color: 'var(--color-yellow)', // Must be CSS variables
          route: { name: 'users' }, // path or route object ({ name: 'route-name',... })
          permission: ALL, // ALL or match ./auth.js
        },
        {
          module: 'university', // Module name for localization
          icon: ['fas', 'graduation-cap'], // Fontawesome or Element UI icon
          color: 'var(--color-yellow)', // Must be CSS variables
          route: { name: 'universities' }, // path or route object ({ name: 'route-name',... })
          permission: ALL, // ALL or match ./auth.js
        },
      ],
    },
  ],
}
