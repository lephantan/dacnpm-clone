import { authActions } from '~/store/auth/actions'
import { Login } from '~/components/common/Auth'
export default {
  layout: 'unauth',
  middleware: 'auth',
  components: {
    Login,
  },
  methods: {
    async postLogin(form) {
      await this.$store.dispatch(authActions.LOGIN, form)
      this.$router.push(`/${this.$store.getters['auth/roleGroup']}`)
      this.$refs.loginForm.reset()
    },
  },
}
