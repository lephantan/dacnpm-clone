export default {
  beforeRouteLeave(to, from, next) {
    this.$confirm(
      this.$t('warning.leave-message'),
      this.$t('warning.leave-title'),
      {
        confirmButtonText: this.$t('warning.leave-ok'),
        cancelButtonText: this.$t('warning.leave-abort'),
        type: 'warning',
      }
    )
      .then(() => {
        next()
      })
      // Choose cancel
      .catch(() => {
        try {
          next(false)
        } catch (err) {
          // Nothing happen
        }
      })
  },
}
